﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace App2
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnOrientationChanged(OrientationChangedEventArgs e)
        {
            base.OnOrientationChanged(e);

            Button btn1 = button1;
            Button btn2 = button2;

            if (e.Orientation == PageOrientation.LandscapeLeft || e.Orientation == PageOrientation.LandscapeRight)
            {
                btn1.Background = new SolidColorBrush(Colors.Yellow);
                btn2.Background = new SolidColorBrush(Colors.Yellow);

                btn1.VerticalAlignment = VerticalAlignment.Top;
                btn2.VerticalAlignment = VerticalAlignment.Top;
            }
            else
            {
                btn1.Background = new SolidColorBrush(Colors.White);
                btn2.Background = new SolidColorBrush(Colors.White);

                btn1.VerticalAlignment = VerticalAlignment.Bottom;
                btn2.VerticalAlignment = VerticalAlignment.Bottom;
            }

        }

    }    
}